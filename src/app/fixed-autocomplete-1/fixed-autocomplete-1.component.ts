import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-fixed-one-autocomplete',
  templateUrl: './fixed-autocomplete-1.component.html',
  styleUrls: ['./fixed-autocomplete-1.component.scss']
})
export class FixedAutocompleteOneComponent {

  title = 'bugfix-mat-option';

  options = [
    {id: 0, title: 'Osef 0'},
    {id: 1, title: 'Osef 1'},
    {id: 2, title: 'Osef 2'},
    {id: 3, title: 'Osef 3'},
    {id: 4, title: 'Osef 4'},
    {id: 5, title: 'Osef 5'},
    {id: 6, title: 'Osef 6'},
    {id: 7, title: 'Osef 7'},
  ];
  filteredOptions = [];
  display = false;

  @ViewChild('inputEl', {static: true})
  inputEl;

  @Output()
  valueSelected = new EventEmitter();

  filter = (val) => {
    this.filteredOptions = this.options.filter(opt => opt.title.toLowerCase().includes(val.toLowerCase()));
    this.display = !!this.filteredOptions.length;
  }

  highlight = (opt, val) => {
    return `<strong>${val}</strong>${opt.title.substr(val.length)}`;
  }

  testBug(opt, event?) {
    this.inputEl.nativeElement.value = opt.title;
    this.valueSelected.emit(opt);
  }

  blur() {
    setTimeout(() => {
      this.display = false;
    }, 500);
  }

}
