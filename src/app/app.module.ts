import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MatCardModule, MatOptionModule} from '@angular/material';
import { BuggyAutocompleteComponent } from './buggy-autocomplete/buggy-autocomplete.component';
import { FixedAutocompleteComponent } from './fixed-autocomplete/fixed-autocomplete.component';
import {FixedAutocompleteOneComponent} from './fixed-autocomplete-1/fixed-autocomplete-1.component';

@NgModule({
  declarations: [
    AppComponent,
    BuggyAutocompleteComponent,
    FixedAutocompleteComponent,
    FixedAutocompleteOneComponent
  ],
  imports: [
    BrowserModule,
    MatCardModule,
    MatOptionModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
